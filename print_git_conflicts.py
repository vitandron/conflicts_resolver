import sys
import traceback
from git import Repo


def commit_or_parents_contains(commit, key):
    if commit.hexsha == key:
        return True
    for parent in commit.parents:
        if commit_or_parents_contains(parent, key):
            return True
    return False


conflicts = {}
repo = Repo(".")

branches = repo.remotes.origin.refs
branches = [ref for ref in branches if ref.name != 'origin/HEAD']

# list branches to find conflicts, None - all branches
# branch_names = ['origin/b1']
branch_names = None
if branch_names is not None:
    branches = [ref for ref in branches if ref.name in branch_names]

# check only children, None - all branches
start_commit = '42225ef48a1801bb56c301b1de5ae37b36fac75c'
# start_commit = None
if start_commit is not None:
    branches = [ref for ref in branches if commit_or_parents_contains(ref.commit, start_commit)]

print('branches to chech:')
print('[')
for i in range(len(branches)):
    print(branches[i].name)
print(']')

if len(branches) > 20:
    print('Too many branches ({}). Process stopped.'.format(len(branches)))
    sys.exit()

for i in range(len(branches)):
    b1 = branches[i]
    for j in range(i + 1, len(branches)):
        b2 = branches[j]
        print("checking conflicts: {} - {}".format(b1, b2))
        repo.git.checkout(b1, force=True)
        try:
            repo.git.merge(b2)
        except Exception as ex:
            traceback.print_exc(file=sys.stdout)

        s = repo.git.ls_files('-u')
        if len(s) > 0:
            confl_files = s.split('\n')
            confl_files = list(set(map(lambda x: x.split()[3], confl_files)))
            print("WARN: branches {} and {} has conflicts".format(b1, b2))
            conflicts['{} - {}'.format(b1, b2)] = confl_files


print('\n----')
# print(conflicts)

if len(conflicts) > 0:
    html = '<p>Conflicts:</p>'
    for key, value in conflicts.items():
        html += '\n<p><strong>branches {}:</strong></p>'.format(key)
        html += '\n<ul>'
        for file in value:
            html += '\n<li>{}</li>'.format(file)
        html += '\n</ul>'

    print(html)
    with open("report.html", "w") as file:
        file.write(html)
