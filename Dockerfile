FROM python:3.6-alpine

RUN apk --update add git openssh && rm -rf /var/lib/apt/lists/* && rm /var/cache/apk/*
RUN pip install gitpython
RUN cp print_git_conflicts.py ../
RUN pwd
RUN python ../print_git_conflicts.py